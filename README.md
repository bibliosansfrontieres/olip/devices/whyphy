# Wifi Connection Tester for Raspberry Pi

This code was initially copied from this place (thanks iiab) :  
https://github.com/iiab/iiab-factory/tree/master/testing/wifi-testing

## The Problem

Different versions of the wifi firmware allow different numbers of maximum wifi connections to hostapd.
This utility helps test how many such connections are allowed for a given configuration.

## Setup

### Raspberry Pi 1 - the tester

- Install Raspberry Pi OS (64 bits lite)
- Make sure rfkill is off, ie: set the country in `raspi-config` (but just the country code, do not set a wifi network, leave the wifi interfaces unconfigured)
- Clone this repository on the Raspberry and cd into it
- Change the wifi SSID in `wpa_iiab.conf` to match the one you want to test
- Execute `sudo ./wifi-test.py` when you are ready to test (you need to initialize the tested Raspberry Pi first)

### Raspberry Pi 2 - the tested

- Install the system to be tested on the Raspberry Pi
- Clone this repository and cd into it
- Execute `./ctl-hostapd` (this will add the following two lines to /etc/hostapd/hostapd.conf  
ctrl_interface=/var/run/hostapd  
ctrl_interface_group=0  
)
- Run `stat-serve.py`

The idea is that the tester is going to try to connect to the wifi of the tested and on top of that it's going to test if connectivity is actually achived.

## Hardware

This utility expects many USB WiFi dongles, which can be ganged together on a USB hub.

## Known Problems

* A Raspberry Pi client can be overwhelmed when USB dongles are attached
* Make sure all dongles are powered off when starting client.
* Power on the USB hubs one at a time.
* It can happen that a few dongles do not connect. Power off and try again.
